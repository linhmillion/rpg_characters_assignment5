﻿using System;
using Xunit;
using RPG_Characters.HeroTypes;

namespace RPG_CharactersTests
{
    public class HeroTypesTests
    {
        [Fact]
        public void Contructor_WhenNewHeroCreate_LevelShouldBeOne()
        {

            //Arrange
            Mage linh = new Mage("Linh Qto");

            //Act
            var actual = linh.Level;

            //Arsert
            Assert.Equal(1, actual);
        }

        [Fact]
        public void Contructor_WhenACharacterLevelUp_LevelShouldBeTwo()
        {
            //Arrange
            Mage linh = new Mage("Linh Qto");
            linh.LevelUp();

            //Act
            var actual = linh.Level;

            //Arsert
            Assert.Equal(2, actual);

        }

        #region 3.TestAtlevel1EachClassHaveDefaultAttribute

        [Fact]
        public void HeroClassMageLevel1_HaveBasicAttributeAsDefault()
        {
            //Arrange
            Mage linh = new Mage("Linh Qto");


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(1, 1, 8);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);
            
            Assert.True(equal);

        }


        [Fact]
        public void HeroClassWarriorLevel1_HaveBasicAttributeAsDefault()
        {
            //Arrange
            Warrior Remco = new Warrior("Remco");


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(5, 2, 1);
            BasicPrimaryAttribute actual = Remco.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);

        }

        [Fact]
        public void HeroClassRangerLevel1_HaveBasicAttributeAsDefault()
        {
            //Arrange
            Ranger Alma = new Ranger("Alma");


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(1, 7, 1);
            BasicPrimaryAttribute actual = Alma.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);

        }

        [Fact]
        public void HeroClassRogueLevel1_HaveBasicAttributeAsDefault()
        {
            //Arrange
            Rogue linh = new Rogue("Linh Qto");


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(2, 6, 1);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);

        }
        #endregion

        #region 4.TestAtLevel2EachClassIncreaseThereAttributeAsExpect

        [Fact]
        public void MageClassWhenLevelUp_HaveThereAttributeIncrease()
        {
            //Arrange
            Mage linh = new Mage("Linh Qto");
            linh.LevelUp();


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(2, 2, 13);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);

        }



        [Fact]
        public void RangerClassWhenLevelUp_HaveThereAttributeIncrease()
        {
            //Arrange
            Ranger linh = new Ranger("Linh Qto");
            linh.LevelUp();


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(2, 12, 2);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);
        }



        [Fact]
        public void RogueClassWhenLevelUp_HaveThereAttributeIncrease()
        {
            //Arrange
            Rogue linh = new Rogue("Linh Qto");
            linh.LevelUp();


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(3, 10, 2);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);
        }


        [Fact]
        public void WarriorClassWhenLevelUp_HaveThereAttributeIncrease()
        {
            //Arrange
            Warrior linh = new Warrior("Linh Qto");
            linh.LevelUp();


            //Act
            BasicPrimaryAttribute expect = new BasicPrimaryAttribute(8, 4, 2);
            BasicPrimaryAttribute actual = linh.BasicPrimaryAttribute;


            //Arsert
            bool equal = expect.Equals(actual);

            Assert.True(equal);
        }

        #endregion
    }
}

