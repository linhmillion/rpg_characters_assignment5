﻿using System;
using System.Collections.Generic;
namespace RPG_Characters.Items
{
    public enum Slot { Head, Legs, Body, Weapon }

    public abstract class Item
    {
        public string Name { get; set; }

        public int RequiredLevel { get; set; } = 1;


        public Slot SlotForEquiped { get; set; }

        public Item()
        {
        }
    }
}

