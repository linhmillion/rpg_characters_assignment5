﻿using System;
namespace RPG_Characters.Items
{
    public class WeaponAttribute
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
       

      


        public WeaponAttribute(double aDamage, double aAttackSpeed)
        {
            Damage = aDamage;
            AttackSpeed = aAttackSpeed;
 

        }


        public static double CalculateDPS(double Damage, double AttackSpeed)
        {
            return Damage * AttackSpeed;
        }

    }
}

