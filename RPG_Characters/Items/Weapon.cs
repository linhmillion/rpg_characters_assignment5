﻿using System;
using RPG_Characters.HeroTypes;
namespace RPG_Characters.Items
{
    public enum WeaponTypes
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands,
    }

    public class Weapon : Item
    {

        public WeaponAttribute WeaponAttribute { get; set; }


        // weapon need a types and can be choose in list types of weapons
        public WeaponTypes Types { get; set; }


        public Weapon()
        {
    
        }
    }
}

