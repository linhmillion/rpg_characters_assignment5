﻿using System;
using RPG_Characters.HeroTypes;

namespace RPG_Characters.Items
{
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate    
    }



    public class Armor : Item
    {


        public BasicPrimaryAttribute ArmorAttribute { get; set; }


        public ArmorTypes Types { get; set; }

        public Armor()
        {
 
        }
    }
}



