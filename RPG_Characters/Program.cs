﻿using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;
using RPG_Characters.Function;
using System.Collections.Generic;
using RPG_Characters.Creation;

namespace RPG_Characters
{
    class Program
    {
        

        static void Main(string[] args)
        {

            // Create some hero


            Mage mage1 = new Mage("Mage1");

            Ranger ranger1 = new("Ranger1");

            Rogue rogue1 = new Rogue("Rouge1");

            Warrior warrior1 = new Warrior("Warrior1");



            // Create some items

            Weapon axes = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Axes,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(12, 0.8)
            };


            Armor seal = new Armor
            {
                Name = "Seal",
                Types = ArmorTypes.Cloth,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Body,
                ArmorAttribute = new BasicPrimaryAttribute(0, 0, 5)
            };


            Armor nana = new Armor
            {
                Name = "Amazing Nana",
                Types = ArmorTypes.Leather,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Legs,
                ArmorAttribute = new BasicPrimaryAttribute(2, 1, 3)
            };


            // check if item is correct
            CheckItemIsCorrect.ItemIsCorrect(nana);
            CheckItemIsCorrect.ItemIsCorrect(axes);
            CheckItemIsCorrect.ItemIsCorrect(seal);


            //Check if this character can collect this item


            CheckpossibleItem.CheckPossibleWeapon(mage1, axes);//not able to select ...

            CheckpossibleItem.CheckPossibleArmor(mage1, seal);// able to select



            // Character can level up
            mage1.LevelUp();


    
            // Select Slot and ADD item in Hero Equiped list if they meet all condition.

            mage1.AddItemToAHero(mage1, Slot.Body, seal);
            
            Console.WriteLine("-----------------------------------------");

            //Calculate total Attribute of a Chararcter and all character equipment

            Console.WriteLine( TotalAttribute.CalculateTotalAttribute(mage1).Strength);



            


            // character damage
            Console.WriteLine(CharacterDamage.CalculateCharacterDamge(mage1));



            Console.WriteLine("-----------------------------------------");

            DisplayInfo.Display(mage1);



            //Console.WriteLine("Slot: {0}, Item: {1}, attribute:{2}", kvp.Key, kvp.Value.Name, ((Armor)kvp.Value).ArmorAttribute.Intelligence);



        }


    }
    
}

