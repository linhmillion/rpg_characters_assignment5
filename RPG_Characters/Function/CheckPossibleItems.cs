﻿using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;

namespace RPG_Characters.Function

{

    public class CheckpossibleItem
    {
        public static void CheckPossibleWeapon(Hero character, Weapon item)
        {

            try
            {
                PossibleWeapon1.PossibleWeapon(character, item);
            }
            catch (InvalidWeaponException ex)
            {
                Console.WriteLine("Exception: " + ex.Message);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic exception: " + ex.Message);
            }



         }




        public static void CheckPossibleArmor(Hero character, Armor item)
        {
            try
            {
                PossibleArmor1.PossibleArmor(character, item);
            }
            catch (InvalidArmorException ex)
            {

                Console.WriteLine("Exception: " + ex.Message);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic exception: " + ex.Message);
            }


        }



    }

}