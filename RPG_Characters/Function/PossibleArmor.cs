﻿using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;

namespace RPG_Characters.Function
{
    public class PossibleArmor1
    { 
        public static bool PossibleArmor(Hero character, Armor item)
        {
            //if character possible to collect this armor through armor type and level.

            if (character.PossibleArmorTypes.Contains(item.Types) && character.Level >= item.RequiredLevel)
            {
                Console.WriteLine("it's possible to collect this Armor");
                return true;
            }
            else
            {
              
                throw new InvalidArmorException();

            }
        }
    }
}

