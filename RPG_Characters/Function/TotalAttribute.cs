﻿using System;
using RPG_Characters.Items;
using RPG_Characters.HeroTypes;



namespace RPG_Characters.Function
{
    public class TotalAttribute
    {
        public static BasicPrimaryAttribute CalculateTotalAttribute(Hero character)
        {


            BasicPrimaryAttribute totalAttribute = new BasicPrimaryAttribute(0, 0, 0);

            foreach (var kvp in character.Equiped)

                // for each value is type Armor add this armor attribute in total attribute.

                if (kvp.Value is Items.Armor)
                {
                    totalAttribute += ((Armor)kvp.Value).ArmorAttribute;

                 


                }


            totalAttribute = totalAttribute + character.BasicPrimaryAttribute;

            return totalAttribute;
        }
    }
}

