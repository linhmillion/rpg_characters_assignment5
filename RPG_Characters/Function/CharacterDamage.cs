﻿using System;
using RPG_Characters.Items;
using RPG_Characters.HeroTypes;

namespace RPG_Characters.Function
{
    public class CharacterDamage
    {


        public static double CalculateCharacterDamge(Hero character)
        {
            double characterDamage = 1;
            double DPS = 0;


            // for each key value pair that equiped in hero character

            foreach (var kvp in character.Equiped)



                // if there is a wepon been equiped the calculate DPS and multible it with character total attribute
                if (kvp.Value is Items.Weapon)
                {
               
                    DPS = WeaponAttribute.CalculateDPS(((Weapon)kvp.Value).WeaponAttribute.Damage, ((Weapon)kvp.Value).WeaponAttribute.AttackSpeed);


                    if (character is HeroTypes.Warrior)
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Strength / 100) + 1) * DPS;
                        
                    }else if (character is HeroTypes.Mage)
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Intelligence / 100) + 1) * DPS;
                    }
                    else
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Dexterity / 100)+ 1) * DPS;
                    }

                }
                else // if character have no weapon or have only armor then DPS by default = 1.
                {

                    DPS = 1;


                    if (character is HeroTypes.Warrior)
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Strength / 100) + 1) * DPS;

                    }
                    else if (character is HeroTypes.Mage)
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Intelligence / 100) + 1) * DPS;
                    }
                    else
                    {
                        characterDamage = ((TotalAttribute.CalculateTotalAttribute(character).Dexterity / 100) + 1) * DPS;
                    }
                }



            return characterDamage;
        }





    }

}

