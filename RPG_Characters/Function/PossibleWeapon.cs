﻿using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;


namespace RPG_Characters.Function
{
    public class PossibleWeapon1
    {
        public static bool PossibleWeapon(Hero character, Weapon item)
            //if character possible to collect this weapon through weapen type and level
        {
            if (character.PossibleWeaponTypes.Contains(item.Types) && character.Level >= item.RequiredLevel)
            {
                Console.WriteLine("it's possible to collect weapon");
                return true;
            }
            else
            {
                
                throw new InvalidWeaponException();

            }
        }
    }
}

