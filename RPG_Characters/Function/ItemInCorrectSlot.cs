﻿using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;

namespace RPG_Characters.Function
{
    public class ItemInCorrectSlot
    {
        public static bool CheckSlotForEquip(Item item)
        {

            // if this item have type of Armor but item slot is weapon then throw an exception
            if (item is Items.Armor && item.SlotForEquiped == Slot.Weapon)
            {
                throw new InvalidSlotForEquipException();
            }

            // if this item have type of Weapon but item slot is armor then throw an exception

            else if (item is Items.Weapon && (item.SlotForEquiped == Slot.Legs || item.SlotForEquiped == Slot.Head || item.SlotForEquiped == Slot.Body))
            {
                throw new InvalidSlotForEquipException();
            }
            else
            {
                Console.WriteLine("This Item is correct");
                return true;
            }

        }


    }
}

