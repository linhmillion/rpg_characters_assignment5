﻿using System;
using RPG_Characters.Items;
using RPG_Characters.CustomException;


namespace RPG_Characters.Function
{
    public class CheckItemIsCorrect
    {

        public static void ItemIsCorrect(Item item)
        {
            try
            {
                ItemInCorrectSlot.CheckSlotForEquip(item);
            }
            catch (InvalidSlotForEquipException ex)
            {

                Console.WriteLine("Exception: " + ex.Message);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Generic exception: " + ex.Message);
            }


        }
    }
}

