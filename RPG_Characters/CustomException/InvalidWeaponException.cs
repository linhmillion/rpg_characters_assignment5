﻿using System;
namespace RPG_Characters.CustomException
{
    
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() : base() { }
        public InvalidWeaponException(string message) : base(message) { }

        public override string Message => "Wrong types of Weapon to select";

    }
}

