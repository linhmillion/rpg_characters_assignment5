﻿using System;
namespace RPG_Characters.CustomException
{
    public class InvalidArmorException :Exception
    {
        public InvalidArmorException() : base() { }
        public InvalidArmorException(string message) : base(message) { }

        public override string Message => "Wrong type of Armor to select";
    }
}

