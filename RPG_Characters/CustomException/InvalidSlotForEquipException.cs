﻿using System;
namespace RPG_Characters.CustomException
{
    public class InvalidSlotForEquipException : Exception
    {
        public InvalidSlotForEquipException() : base() { }
        public InvalidSlotForEquipException(string message) : base(message) { }

        public override string Message => "This type of item can't equip for this slot";
    }
}

