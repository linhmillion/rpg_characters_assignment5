﻿using System;
using RPG_Characters.Items;

namespace RPG_Characters.HeroTypes
{
    public class Ranger: Hero
    {
        public static BasicPrimaryAttribute StartPoint = new BasicPrimaryAttribute(1, 7, 1);
        


        public Ranger(string name)
        {
            Name = name;

            PossibleWeaponTypes.AddRange(new WeaponTypes[] { WeaponTypes.Bows});

            PossibleArmorTypes.AddRange(new ArmorTypes[] { ArmorTypes.Leather, ArmorTypes.Mail });


            BasicPrimaryAttribute = Ranger.StartPoint;
            
     
        }


        public override BasicPrimaryAttribute LevelUp()
        {
            BasicPrimaryAttribute LevelUpAttribute = new BasicPrimaryAttribute(1, 5, 1);
            Level += 1;
            return BasicPrimaryAttribute = BasicPrimaryAttribute + LevelUpAttribute;
        }
    }
}

