﻿using System;
using RPG_Characters.Items;

namespace RPG_Characters.HeroTypes
{
    public class Warrior: Hero
    {
        public static BasicPrimaryAttribute StartPoint = new BasicPrimaryAttribute(5, 2, 1);

        


        public override BasicPrimaryAttribute LevelUp()
        {
            BasicPrimaryAttribute LevelUpAttribute = new BasicPrimaryAttribute(3, 2, 1);
            Level += 1;
            return BasicPrimaryAttribute = BasicPrimaryAttribute + LevelUpAttribute;
        }

        public Warrior(string name)
        {
            Name = name;
         
            BasicPrimaryAttribute = Warrior.StartPoint;

            PossibleWeaponTypes.AddRange(new WeaponTypes[] { WeaponTypes.Axes, WeaponTypes.Hammers, WeaponTypes.Swords });

            PossibleArmorTypes.AddRange(new ArmorTypes[] { ArmorTypes.Plate, ArmorTypes.Mail });

        }
    }
}

