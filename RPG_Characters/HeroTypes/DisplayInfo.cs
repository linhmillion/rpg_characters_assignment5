﻿using System;
using RPG_Characters.Items;
using RPG_Characters.Function;
using RPG_Characters.CustomException;



namespace RPG_Characters.HeroTypes
{
    public class DisplayInfo
    {

        public static void Display(Hero character)
        {
            Console.WriteLine($"Name: {character.Name}");
            Console.WriteLine($"Level: {character.Level}");
            Console.WriteLine($"Strength: {TotalAttribute.CalculateTotalAttribute(character).Strength}");
            Console.WriteLine($"Dexterity: {TotalAttribute.CalculateTotalAttribute(character).Dexterity}");
            Console.WriteLine($"Intelligence: {TotalAttribute.CalculateTotalAttribute(character).Intelligence}");
            Console.WriteLine($"Damage: {CharacterDamage.CalculateCharacterDamge(character)}");
            //Console.WriteLine($"Intelligence: {}");
        }


    }
}

