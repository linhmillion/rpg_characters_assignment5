﻿using System;
using System.Collections.Generic;
namespace RPG_Characters.HeroTypes
{
    public class BasicPrimaryAttribute 
    {

        public double Strength { get; set; } = 0;

        public double Dexterity { get; set; } = 0;

        public double Intelligence { get; set; } = 0;


        public BasicPrimaryAttribute(double strength, double dexterity, double intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public static BasicPrimaryAttribute operator +(BasicPrimaryAttribute lhs, BasicPrimaryAttribute rhs)
        {
            return new BasicPrimaryAttribute(lhs.Strength + rhs.Strength, lhs.Dexterity + rhs.Dexterity, lhs.Intelligence + rhs.Intelligence);

        }


        public override bool Equals(object obj)
        {
            return obj is BasicPrimaryAttribute basicPrimaryAttribute &&
            Strength == basicPrimaryAttribute.Strength &&
            Dexterity == basicPrimaryAttribute.Dexterity &&
            Intelligence == basicPrimaryAttribute.Intelligence;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence);
        }


    }

        
}

