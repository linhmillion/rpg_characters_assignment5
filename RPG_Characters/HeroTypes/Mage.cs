﻿using System;
using RPG_Characters.Items;


namespace RPG_Characters.HeroTypes
{
    public class Mage: Hero
    {
        public static BasicPrimaryAttribute StartPoint = new BasicPrimaryAttribute(1, 1, 8);
        

        public override BasicPrimaryAttribute LevelUp()
        {
            BasicPrimaryAttribute LevelUpAttribute = new BasicPrimaryAttribute(1, 1, 5);
            Level += 1;
            return BasicPrimaryAttribute = BasicPrimaryAttribute + LevelUpAttribute;
        }


        public Mage(string name)
        {
    

            Name = name;

            BasicPrimaryAttribute = Mage.StartPoint;

            PossibleWeaponTypes.AddRange(new WeaponTypes[] { WeaponTypes.Staffs, WeaponTypes.Wands });
            PossibleArmorTypes.AddRange(new ArmorTypes[] { ArmorTypes.Cloth });


        }

    }
}

    