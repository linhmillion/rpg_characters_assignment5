﻿using System;
using RPG_Characters.Items;


namespace RPG_Characters.HeroTypes
{
    public class Rogue: Hero
    {
        public static BasicPrimaryAttribute StartPoint = new BasicPrimaryAttribute(2, 6, 1);
        

        public override BasicPrimaryAttribute LevelUp()
        {
            BasicPrimaryAttribute LevelUpAttribute = new BasicPrimaryAttribute(1, 4, 1);
            Level += 1;
            return BasicPrimaryAttribute += LevelUpAttribute;
        }


        public Rogue(string name)
        {
            
            Name = name;
        
            BasicPrimaryAttribute = Rogue.StartPoint;

            PossibleWeaponTypes.AddRange(new WeaponTypes[] { WeaponTypes.Daggers, WeaponTypes.Swords });
            PossibleArmorTypes.AddRange(new ArmorTypes[] { ArmorTypes.Leather, ArmorTypes.Mail });



        }
    }
}

