﻿using System;
using System.Collections.Generic;
using RPG_Characters.Items;
using RPG_Characters.Function;
using RPG_Characters.CustomException;



namespace RPG_Characters.HeroTypes
{
    public abstract class Hero
    { 

        public string Name { get; set; }

        public int Level { get; set; } = 1;

        public BasicPrimaryAttribute BasicPrimaryAttribute { get; set; }

        public List<WeaponTypes> PossibleWeaponTypes = new List<WeaponTypes>();

        public List<ArmorTypes> PossibleArmorTypes = new List<ArmorTypes>();

        Dictionary<Slot, Item> equipment = new();

        public Dictionary<Slot, Item> Equiped { get => equipment; }




        public void AddItem(Slot slot, Item item)
        {
            // If Slot is available and slot is correct with slot for equip then can add 

            if (Equiped.ContainsKey(slot))
            {
                Console.WriteLine($"This {slot} already have item");

            }
            else if (item.SlotForEquiped != slot)
            {
                Console.WriteLine($"Can't select this item for this {slot}");
            }

            else
            {
                Equiped.Add(slot, item);
                

            }
        }

        //If this hero select a armor or weapon, then check if this possible to select with type of hero and correct equiped to equip.

        public string AddItemToAHero(Hero character, Slot slot, Item item)
        {

            string A = "";


            if (item is Items.Armor)
            {
                if (PossibleArmor1.PossibleArmor(character, (Armor)item) && ItemInCorrectSlot.CheckSlotForEquip(item))
                {
                    character.AddItem(slot, item);
                    
                    A = "New armor equipped!";

                }

            }
            else if (item is Items.Weapon)
            {
                if (PossibleWeapon1.PossibleWeapon(character, (Weapon)item) && ItemInCorrectSlot.CheckSlotForEquip(item))
                {
                    character.AddItem(slot, item);
                    A = "New weapon equipped!";

                }

            }
            else
            {

                A = "unable to add this item";
            }

            Console.WriteLine(A);
            return A;
        }

        



        public abstract BasicPrimaryAttribute LevelUp();


   
    }
}

