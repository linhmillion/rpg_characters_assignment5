﻿
using System;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.CustomException;
using RPG_Characters.Function;
using System.Collections.Generic;



namespace RPG_Characters.Creation
{
    public class CreateItems
    {
        Weapon axes = new Weapon
        {
            Name = "Big Weapon",
            Types = WeaponTypes.Axes,
            RequiredLevel = 1,
            SlotForEquiped = Slot.Weapon,
            WeaponAttribute = new WeaponAttribute(12, 0.8)
        };

        Armor seal = new Armor
        {
            Name = "Seal",
            Types = ArmorTypes.Cloth,
            RequiredLevel = 1,
            SlotForEquiped = Slot.Body,
            ArmorAttribute = new BasicPrimaryAttribute(0, 0, 5)
        };
    }
}

