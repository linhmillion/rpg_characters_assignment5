﻿using System;
using Xunit;
using RPG_Characters.HeroTypes;
using RPG_Characters.Items;
using RPG_Characters.Function;
using RPG_Characters.CustomException;

namespace RPG_ItemsTest
{
    public class ItemsTest
    {

        #region 1-4.WhenShouldThrowInvalidWeapon_OrInvalidArmorException
        [Fact]
        public void CharacterAddHigherWeaponThanAllow_ShoudlThrowInvalidException()
        {

            //Arrange
            Warrior Allin = new Warrior("Allin warior");
            Weapon Bigaxes = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Axes,
                RequiredLevel = 2,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(12, 0.8)
            };


            //Act and assert
            Assert.Throws<InvalidWeaponException>(() => PossibleWeapon1.PossibleWeapon(Allin, Bigaxes));

        }


        [Fact]
        public void CharacterAddHigherArmorThanAllow_ShoudlThrowInvalidException()
        {

            //Arrange
            Warrior Allin = new Warrior("Allin warior");
            Armor Plate = new Armor
            {
                Name = "Big Plate",
                Types = ArmorTypes.Plate,
                RequiredLevel = 2,
                SlotForEquiped = Slot.Body,
                ArmorAttribute = new BasicPrimaryAttribute(1, 0, 0)
            };


            //Act and assert
            Assert.Throws<InvalidArmorException>(() => PossibleArmor1.PossibleArmor(Allin, Plate));

        }


        [Fact]
        public void CharacterAddWrongWeaponType_ShoudlThrowInvalidException()
        {

            //Arrange
            Warrior Allin = new Warrior("Allin warior");
            Weapon Bow = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Bows,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(12, 0.8)
            };


            //Act and assert
            Assert.Throws<InvalidWeaponException>(() => PossibleWeapon1.PossibleWeapon(Allin, Bow));

        }

        // Use the warrior and the cloth armor.
        [Fact]
        public void CharacterAddWrongArmorTypes_ShoudlThrowInvalidException()
        {

            //Arrange
            Warrior Allin = new Warrior("Allin warior");
            Armor Cloth = new Armor
            {
                Name = "Big Cloth",
                Types = ArmorTypes.Cloth,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Body,
                ArmorAttribute = new BasicPrimaryAttribute(1, 0, 0)
            };


            //Act and assert
            Assert.Throws<InvalidArmorException>(() => PossibleArmor1.PossibleArmor(Allin, Cloth));

        }


        #endregion


        #region 5-6.ReturnMessageWhenAddedItemSuccess

        //If a character equips a valid weapon, a success message should be returned
        [Fact]
        public void CharacterSuccessEquipAWeapon_ShouldShowMessage()
        {
            //Arrange
            Warrior Allin = new Warrior("Allin warior");
            Weapon Bigaxes = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Axes,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(12, 0.8)
            };



            string result = "New weapon equipped!";
            string actual = Allin.AddItemToAHero(Allin, Slot.Weapon, Bigaxes);


            Assert.Same(result, actual);



        }

        [Fact]
        public void CharacterSuccessEquipAArmor_ShouldShowMessage()
        {
            //Arrange
            Warrior Allin = new Warrior("Allin warior");

            Armor Plate = new Armor
            {
                Name = "Big Plate",
                Types = ArmorTypes.Plate,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Body,
                ArmorAttribute = new BasicPrimaryAttribute(1, 0, 0)
            };



            Allin.AddItemToAHero(Allin, Slot.Body, Plate);

            string result = "New armor equipped!";
            string actual = Allin.AddItemToAHero(Allin, Slot.Weapon, Plate);


            Assert.Same(result, actual);



        }
        #endregion

        #region 7-9.CalculateFunction

        [Fact]
        public void CalculateDamge_WhenNoWeaponEquiped()
        {

            //arrange
            Warrior Allin = new Warrior("Allin warior");

            // Act 
            double actual = CharacterDamage.CalculateCharacterDamge(Allin);
            double expect = 1 * (1 + (5 / 100));

            // Assert
            Assert.Equal(expect, actual);

        }



        [Fact]
        public void CalculateDamge_WhenWeaponEquiped()
        {

            //arrange
            Warrior Allin1 = new Warrior("Allin warior");
            Weapon Bigaxes = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Axes,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(7, 1.1)
            };

            Allin1.AddItemToAHero(Allin1, Slot.Weapon, Bigaxes);
            // Act 
            double actual = CharacterDamage.CalculateCharacterDamge(Allin1);
            double expect = (7.0 * 1.1) * (1.0 + (5.0 / 100));

            // Assert
            Assert.Equal(expect, actual);

        }

        [Fact]
        public void CalculateDamge_WithValidWeaponAndArmor()
        {

            //arrange
            Warrior Allin1 = new Warrior("Allin warior");


            Weapon Bigaxes = new Weapon
            {
                Name = "Big Weapon",
                Types = WeaponTypes.Axes,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Weapon,
                WeaponAttribute = new WeaponAttribute(7, 1.1)
            };



            Armor Plate = new Armor
            {
                Name = "Big Plate",
                Types = ArmorTypes.Plate,
                RequiredLevel = 1,
                SlotForEquiped = Slot.Body,
                ArmorAttribute = new BasicPrimaryAttribute(1, 0, 0)
            };



            Allin1.AddItemToAHero(Allin1, Slot.Body, Plate);

            Allin1.AddItemToAHero(Allin1, Slot.Weapon, Bigaxes);

            // Act 
            double actual = CharacterDamage.CalculateCharacterDamge(Allin1);
            double expect = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100));

            // Assert
            Assert.Equal(expect, actual);

        }


        #endregion
    }
}

